<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$families = Family::all();
        $families = Family::paginate(2);
        if ($request->ajax()) {
            return $families;
        } else {
            return view('family.index', ['families' => $families]);
        }
    }


    public function ajax()
    {
        return view('family.ajax');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'code' => 'required|unique:families|max:4',
        'name' => 'required|max:40'
        ]);

        $family = new Family($request->all());
        $family->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        return  view('family.show', ['family']);
//      $family = Family::find(1);    
        $family = Family::findOrFail($id);
        
        if ($request->ajax()) {
            return $family;
        } else {
            return view('family.show', ['family'=>$family]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family = Family::findOrFail($id);
        return view('family.edit', ['family'=>$family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
        'code' => 'required|unique:families|max:4',
        'name' => 'required|max:40'
        ]);
        $family = Family::findOrFail($id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();

        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }

        //dd($family);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::find($id)->delete();
        //Family::destroy($id);
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/families');
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'code' => 'RAIN',
            'name' => 'Ratón inalámbrico',
            'price' => 10.23,
            'family_id' => 2,
        ]);
        DB::table('products')->insert([
            'code' => 'ININ',
            'name' => 'Intel Inside',
            'price' => 56.48,
            'family_id' => 1,
        ]);
        DB::table('products')->insert([
            'code' => 'ALDI',
            'name' => 'Western Digital',
            'price' => 80.23,
            'family_id' => 3,
        ]);
        DB::table('products')->insert([
            'code' => 'MULT',
            'name' => 'Joystick',
            'price' => 20.56,
            'family_id' => 4,
        ]);
    }
}

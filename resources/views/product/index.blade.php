@extends('layouts.app')


@section('content')
    <h1>Lista de productos</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Código</th>
                <th>Nombre</th>
            </tr>
        </thead>

        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{ $product['id'] }}</td>
            <td>{{ $product['code'] }}</td>
            <td>{{ $product['name'] }}</td>
            <td>

                <form method="post" action="/families/{{ $family->id }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Borrar">
                    <a href="/families/{{ $family->id }}/edit">Editar</a>
                    <a href="/families/{{ $family->id }}">Ver</a>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
        </table>
    <p><a href="/families/create">Nuevo</a></p> 
    {!! $families->render() !!}    
@stop
